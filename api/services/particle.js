var Particle = require('particle-api-js');
var p = new Particle();
var token,
    device;

module.exports = {
    login: function (cb) {
        var user = {username: 'sgmadsen@gmail.com', password: 'scaughtty22'};
        p.login(user).then(function(data){
                token = data.body.access_token;
                cb();
            }, function(err) {
                console.log('API call completed on promise fail: ', err);
            }
        );
    },
    list: function (next) {
        var devices = p.listDevices({ auth: token });
        devices.then(
            function(devices){
                console.log('Devices: ', devices);
                next();
            },
            function(err) {
                console.log('List devices call failed: ', err);
            }
        );
    },
    fn: function (name, argument, next) {
        var fnPr = p.callFunction({ deviceId: device, name: name, argument: argument, auth: token });

        fnPr.then(
            function(data) {
                console.log('Function called succesfully:', data);
                next(null, data);
            }, function(err) {
                console.log('An error occurred:', err);
            });
    },
    claim: function (deviceId, next) {
        p.claimDevice({ deviceId: deviceId, auth: token }).then(function(data) {
            console.log('device claim data:', data);
            device = deviceId;
            next(data);
        }, function(err) {
            console.log('device claim err:', err);
        });
    },
    getVariable: function (name, next) {
        p.getVariable({deviceId: device, name: name, auth: token}).then(function (data) {
            next(null, data);
        }, function (err) {
            console.log(err);
        })
    }
};