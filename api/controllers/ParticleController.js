/**
 * ParticleController
 *
 * @description :: Server-side logic for managing Particles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	send: function (req, res) {
        async.waterfall([
            function (next) {
                sails.services.particle.fn(req.param('name'), req.param('argument'), next);
            },
            function (data, next) {
                console.log(data);
                res.json(data);
            }
        ])
    },
    claim: function (req, res) {
        particle.claim(req.param('deviceId'), function (data) {
            console.log(data);
            res.json(data);
        })
    },
    getVariable: function (req, res) {
        async.waterfall([
            function (next) {
                sails.services.particle.getVariable(req.param('name'), next)
            },
            function (data, next) {
                res.json(data);
            }
        ])
    }
};

