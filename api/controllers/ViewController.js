/**
 * ViewController
 *
 * @description :: Server-side logic for managing Views
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	


  /**
   * `ViewController.devices()`
   */
  devices: function (req, res) {
    async.waterfall([
        function (next) {
            sails.services.particle.list(next);
        },
        function (devices, next) {
            console.log(devices);
            res.locals.devices = devices;
            res.view('devices');
        }
    ]);
  },


  /**
   * `ViewController.account()`
   */
  account: function (req, res) {
      async.waterfall([
          function (next) {
              sails.services.particle.getVariable('servos', next);
          },
          function (servos, next) {
              res.locals.servos = servos.body.result;
              sails.services.particle.getVariable('home', next);
          },
          function (home, next) {
              res.locals.home = home.body.result;
              sails.services.particle.getVariable('scale', next);
          },
          function (scale, next) {
              res.locals.scale = scale.body.result;
              res.view('account');
          }
      ]);
  },

    /**
     * `ViewController.account()`
     */
    controls: function (req, res) {
        res.view('controls');
    }

};

